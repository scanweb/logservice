﻿
namespace ScanwebLogsListener
{
    public class LoggerType
    {
        public const string Root = "root";
        public const string Critical = "scanweb-critical";
        public const string Ows = "scanweb-ows";
        public const string General = "scanweb-info";
        public const string OnlinePaymentInfoLogger = "scanweb-onlinepayment-info";
        public const string OnlinePaymentFatalLogger = "scanweb-onlinepayment-critical";
        public const string OnlinePaymentTransactionsInfoLogger = "scanweb-onlinepaymenttransactions-info";
        public const string SessionNotValidLogger = "scanweb-sessionnotvalidscenarios-info";
        public const string SmtpAppenderLogger = "scanweb-smtpappenderlogger-info";
        public const string ReservationRatesLogger = "scanweb-reservationrateslogger-info";
        public const string CSVLogger = "csv-logger";
    }
}
