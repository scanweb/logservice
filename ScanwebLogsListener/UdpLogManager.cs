﻿

using System;
using System.Globalization;
using System.Xml;
using System.Configuration;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using log4net;
using log4net.Config;
using System.Diagnostics;

namespace ScanwebLogsListener
{
    public sealed class UdpLogManager : BaseLogger
    {
        private static readonly UdpLogManager LogManagerinstance = new UdpLogManager();
        private static UdpClient _listener;

        public static UdpLogManager Instance
        {
            get { return LogManagerinstance; }
        }

        public bool ServiceStarted { get; set; }

        private UdpLogManager()
        {
            XmlConfigurator.Configure();
            RootLogger = LogManager.GetLogger(LoggerType.Root);
            FatalLogger = LogManager.GetLogger(LoggerType.Critical);
            OwsLogger = LogManager.GetLogger(LoggerType.Ows);
            GeneralInfoLogger = LogManager.GetLogger(LoggerType.General);
            OnlinePaymentInfoLogger = LogManager.GetLogger(LoggerType.OnlinePaymentInfoLogger);
            OnlinePaymentFatalLogger = LogManager.GetLogger(LoggerType.OnlinePaymentFatalLogger);
            OnlinePaymentTransactionsInfoLogger = LogManager.GetLogger(LoggerType.OnlinePaymentTransactionsInfoLogger);
            SessionNotValidLogger = LogManager.GetLogger(LoggerType.SessionNotValidLogger);
            SmtpAppenderLogger = LogManager.GetLogger(LoggerType.SmtpAppenderLogger);
            ReservationRatesLogger = LogManager.GetLogger(LoggerType.ReservationRatesLogger);
            CSVLogger = LogManager.GetLogger(LoggerType.CSVLogger);
        }

        public void ProcessLogs()
        {
            try
            {
                InitialAsynCallWaitTime();

                var port = Int32.Parse(ConfigurationManager.AppSettings["PortNumber"]);
                var configIp = Int64.Parse(ConfigurationManager.AppSettings["IPAddress"]);
                var ipAddress = configIp == 0 ? IPAddress.Any : new IPAddress(configIp);
                _listener = new UdpClient(port);
                var groupEp = new IPEndPoint(ipAddress, port);

                while (ServiceStarted)
                {
                    var bytes = _listener.Receive(ref groupEp);
                    var loggingEvent = Encoding.UTF8.GetString(bytes, 0, bytes.Length);
                    FilterLogMessage(loggingEvent);
                }
            }
            catch (Exception exception)
            {
                EventLog.WriteEntry(exception.Message, exception.StackTrace);
            }
        }

        public void StopProcessingLogs()
        {
            if (_listener != null)
                _listener.Close();
        }

        private void InitialAsynCallWaitTime()
        {
            var waitTime = Int32.Parse(ConfigurationManager.AppSettings["ServiceStartupTime"]);
            Thread.Sleep(waitTime);
        }

        private void FilterLogMessage(string logMessage)
        {
            var document = new XmlDocument();
            try
            {
                document.LoadXml(logMessage);
                var loggerTypeXPath = ConfigurationManager.AppSettings["LogTypeXPath"].ToString(CultureInfo.InvariantCulture);
                var node = document.SelectSingleNode(loggerTypeXPath);
                var loggerType = node != null ? node.InnerText.ToLower() : string.Empty;
                switch (loggerType)
                {
                    case LoggerType.Critical:
                        FatalLogger.Info(logMessage);
                        break;
                    case LoggerType.Root:
                        RootLogger.Info(logMessage);
                        break;
                    case LoggerType.Ows:
                        OwsLogger.Info(logMessage);
                        break;
                    case LoggerType.General:
                        GeneralInfoLogger.Info(logMessage);
                        break;
                    case LoggerType.OnlinePaymentInfoLogger:
                        OnlinePaymentInfoLogger.Info(logMessage);
                        break;
                    case LoggerType.OnlinePaymentFatalLogger:
                        OnlinePaymentFatalLogger.Info(logMessage);
                        break;
                    case LoggerType.OnlinePaymentTransactionsInfoLogger:
                        OnlinePaymentTransactionsInfoLogger.Info(logMessage);
                        break;
                    case LoggerType.SessionNotValidLogger:
                        SessionNotValidLogger.Info(logMessage);
                        break;
                    case LoggerType.SmtpAppenderLogger:
                        SmtpAppenderLogger.Info(logMessage);
                        break;
                    case LoggerType.ReservationRatesLogger:
                        ReservationRatesLogger.Info(logMessage);
                        break;
                    case LoggerType.CSVLogger:
                        var csvNode = document.SelectSingleNode("//Message");
                        var csvLogText = csvNode != null ? csvNode.InnerText : string.Empty;
                        CSVLogger.Info(csvLogText);
                        break;
                }
            }
            catch (Exception ex)
            { }
            finally
            {
                document = null;
            }
        }
    }
}