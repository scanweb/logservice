﻿using System;
using System.ServiceProcess;

namespace ScanwebLogsListener
{
    public partial class ScanwebLogListener : ServiceBase
    {
        private readonly UdpLogManager _udpLogManager;

        private delegate void AsyncUdpLogManager();
        private AsyncUdpLogManager _asyncUdpLogManager;
        private IAsyncResult _asyncResult;

        public ScanwebLogListener()
        {
            InitializeComponent();
            _udpLogManager = UdpLogManager.Instance;
           
        }

        protected override void OnStart(string[] args)
        {
             _udpLogManager.ServiceStarted = true;
            _asyncUdpLogManager = _udpLogManager.ProcessLogs;
            _asyncResult = _asyncUdpLogManager.BeginInvoke(null, null);
        }

        protected override void OnStop()
        {
            if (_udpLogManager != null)
            {
                _udpLogManager.ServiceStarted = false;
                _udpLogManager.StopProcessingLogs();
            }

            if (_asyncUdpLogManager != null && !_asyncResult.IsCompleted)
                _asyncUdpLogManager.EndInvoke(_asyncResult);
        }
    }
}
