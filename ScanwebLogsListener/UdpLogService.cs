﻿using System.ServiceProcess;

namespace ScanwebLogsListener
{
    static class UdpLogService
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] 
			{ 
				new ScanwebLogListener() 
			};
            ServiceBase.Run(ServicesToRun);
        }
    }
}
