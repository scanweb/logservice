﻿
using log4net;

namespace ScanwebLogsListener
{
    public class BaseLogger
    {
            protected static ILog RootLogger;
            protected static ILog FatalLogger;
            protected static ILog OwsLogger;
            protected static ILog GeneralInfoLogger;
            protected static ILog OnlinePaymentInfoLogger;
            protected static ILog OnlinePaymentFatalLogger;
            protected static ILog OnlinePaymentTransactionsInfoLogger;
            protected static ILog SessionNotValidLogger;
            protected static ILog SmtpAppenderLogger;
            protected static ILog ReservationRatesLogger;
            protected static ILog CSVLogger;
    }
}
